Ski~nusoïde
===========

Ski~nusoïde is a game inspired by *run and gun*, *water-skiing* and *arcade*
games. You control the amplitude and frequency of a sine wave to dodge
obstacles.

The original Ski~nusoïde game has been developed during the
[2017 Global Game Jam](https://globalgamejam.org/2017/games/skinuso%C3%AFde).

You can take a look at the current development version on
[kao98.gitlab.io/skinusoide](https://kao98.gitlab.io/skinusoide). Control the
sine with your mouse pointer by pressing your main mouse button, or using your
keyboard: Z Q S D, or the arrow keys.


## Current Status

At the moment, the game is nearly as it was at the end of the Game Jam. It just
misses management of the high scores, and a proper UI. First tests to get some
gamer feedbacks will start soon.

It is planned to be playable on mobile devices either. It is actually the case
already, but the UI need some improvements.


## What's next

It is planned to improved the graphics, the mobile experience, the gameplay, and
to add some social features - based on ActivityPub - for sharing its best scores
with friends.

On a technical side, it is planned to implements some features using Rust and
WebAssembly to improve performances. Also, mainly due to future social
capabilities, a server-side of the game will be required.


## How to deal with the source code

[NodeJS / NPM](https://nodejs.org/) are required.

Once you have cloned the repository, run the following commands:
```shell
# to setup the project and install the dependencies
npm install

# build the entire project a first time
npm build

# or to build only the the sass files
npm build-sass

# then you can start a live-reload server at 127.0.0.1:9966
npm run serve
```

If you browse to http://127.0.0.1:9966, the game should load and be playable. If
you do any modification in the sources, the browser should reload automatically.

Note that if you make any update in the scss files, you have to build css files
yourself using `npm run build-sass`. Those are not watched by `npm serve`.

Other commands may be usefull:
```shell
# to build the bundle from the sources. That will build css files from scss, and
# typescript files into a bundle.js that you can directly load in an HTML page.
npm run build

# it is possible to build the sass files and the typescript files independently:
npm run build-sass
npm run build-ts

# If you want to use your own web server, but sill watch for changes in the
# sources, the following will build a bundle.js automatically each time any ts
# file has been udpated. Note: nothing else than typescript is watch and built.
npm run watch
```

## How to debug from Visual Studio Code

Modern browser embedded powerfull debugger tools. However debugging typescript
code from Visual Studio Code can be usefull sometimes.

### Using Firefox

Install the [Debugger for Firefox](https://marketplace.visualstudio.com/items?itemName=firefox-devtools.vscode-firefox-debug)
extension for Visual Studio Code, and add a file `launch.json` into the
`.vscode` with something like the following:
```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Debug from Firefox",
      "type": "firefox",
      "request": "launch",
      "reAttach": true,
      "url": "http://127.0.0.1:9966/index.html",
      "webRoot": "${workspaceFolder}/public",
      "pathMappings": [
        {
          "url": "http://127.0.0.1:9966/js/src",
          "path": "${workspaceFolder}/src"
        }
      ]
    }
  ]
}
```

Then run the server using `npm serve`, then start the debugging using
`Debug from Firefox`: you should be able to debug your code using the Visual
Studio Debugger.


### Using Chrome

As for Firefox, download the appropriate debugger extension, and add a proper
`launch.json`. You can take a look on the one for Firefox for inspiration.


## Contributing

**Opening [issues](https://gitlab.com/kao98/skinusoide/-/issues)** to share your
feedbacks, issues, bugs, and anything else **is greatly welcome**.

If you want to contribute to the code, **fork the project, then submit merge
requests**. The game is built upon [CreateJS](https://createjs.com/).

Please **use EditorConfig** ide plug-in for consistent development environment, and
make sure to **properly lint your code using eslint**.

The sources are organized in the following way:

- `public`: all files that are served as-is, and files that have been built
- `scss`: scss files - they are built using sass into public/css
- `src`: typescript sources - they are built into public/js/bundle.js
- `public/assets`: UI and Game assets like images, sounds, ...
- `public/css`: stylesheets for the HTML UI - generated from scss files
- `public/js`: third-party libraries (create.min.js, ...) and resulting bundle
- `public/manifests`: manifests files for the preloader

There are two main parts: UI and Game.

- UI referes to the GUI (the index.html home page, the menus, ...) and to user
  interactions (clicks, key press, mousemove, touchmove, ...)
- The Game is the typescript game itself using [CreateJS](https://createjs.com/)

