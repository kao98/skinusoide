export interface HSL {
  hue: number
  sat: number
  lit: number
}

export class HSLColor {
  hsl: HSL

  constructor (hue?: number, sat?: number, lit?: number) {
    this.hsl = {
      hue,
      lit,
      sat
    }
  }

  hslCssColor (): string {
    return HSLColor.hslCssColor(this.hsl)
  }

  static hslCssColor (color: HSL): string {
    return `hsl(${color.hue}, ${color.sat}%, ${color.lit}%)`
  }

  get hue (): number {
    return this.hsl.hue
  }

  set hue (value: number) {
    this.hue = value
  }

  get sat (): number {
    return this.hsl.sat
  }

  set sat (value: number) {
    this.sat = value
  }

  get lit (): number {
    return this.hsl.lit
  }

  set lit (value: number) {
    this.lit = value
  }
}
