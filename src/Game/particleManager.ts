interface IParticle {
  reset (x?: number, y?: number): void
}

export default class ParticleManager {
  particleCreationMethod: () => IParticle
  isCandidateMethod: (particle: IParticle) => boolean
  particleUpdateMethod: (particle: IParticle) => void

  private particleArray: Array<IParticle>
  private nextCreationTime: number
  private time: number
  private delay: number
  private maximumParticlesAllowed: number

  constructor (maximumParticlesAllowed: number, delayBetweenCreation = 0) {
    this.time = 0
    this.nextCreationTime = 0
    this.nextDelay = delayBetweenCreation
    this.maximumParticlesAllowed = maximumParticlesAllowed
    this.particleArray = []
  }

  update (time: number, x?: number, y?: number): IParticle | undefined {
    this.setTime(time)

    if (!this.timeToCreateNewParticle()) {
      return
    }

    const particle = this.updateParticles(x, y)

    this.setNextCreationTime()

    return particle
  }

  private setTime (time: number): void {
    this.time = time
  }

  private timeToCreateNewParticle (): boolean {
    return this.time > this.nextCreationTime
  }

  private updateParticles (x?: number, y?: number): IParticle | undefined {
    if (this.canCreateNewParticle()) {
      const particle = this.particleCreationMethod()

      this.particles.push(particle)

      return particle
    }
    const particle = this.findCandidate()

    particle?.reset(x, y)

    return particle
  }


  private setNextCreationTime (): void {
    this.nextCreationTime = this.time + this.nextDelay
  }

  private canCreateNewParticle (): boolean {
    return this.particles.length < this.maximumParticlesAllowed
  }

  private findCandidate (): IParticle | undefined {
    return this.particles.find(this.isCandidateMethod)
  }

  set nextDelay (delay: number) {
    this.delay = delay
  }

  get nextDelay (): number {
    return this.delay
  }

  get particles (): Array<IParticle> {
    return this.particleArray
  }
}
