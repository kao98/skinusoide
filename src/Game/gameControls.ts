
import Game from './game'

const
  AMPLITUDE_STEP = 5,
  FREQUENCY_STEP = 0.01

interface SineControls {
  decreasing: boolean,
  increasing: boolean,
  increment: number,
  step: number
}

interface PointerControls {
  current: createjs.Point,
  gap: createjs.Point,
  origin: createjs.Point
}

export default class GameControls {
  amplitude: SineControls
  frequency: SineControls
  pointer: PointerControls

  constructor (private readonly gameInstance: Game) {
    this.amplitude = {
      decreasing: false,
      increasing: false,
      increment: 0,
      step: AMPLITUDE_STEP
    }

    this.frequency = {
      decreasing: false,
      increasing: false,
      increment: 0,
      step: FREQUENCY_STEP
    }

    this.pointer = {
      current: new createjs.Point(),
      gap: null,
      origin: new createjs.Point()
    }
  }

  handleKeydown (key: string, _code: string): void {
    switch (key) {
    case 'ArrowUp':
    case 'z':
      this.amplitude.increasing = true
      break

    case 'ArrowDown':
    case 's':
      this.amplitude.decreasing = true
      break

    case 'ArrowRight':
    case 'd':
      this.frequency.increasing = true
      break

    case 'ArrowLeft':
    case 'q':
      this.frequency.decreasing = true
      break
    default:
    }
  }

  handleKeyup (key: string, _code: string): void {
    switch (key) {
    case 'ArrowUp':
    case 'z':
      this.amplitude.increasing = false
      break

    case 'ArrowDown':
    case 's':
      this.amplitude.decreasing = false
      break

    case 'ArrowRight':
    case 'd':
      this.frequency.increasing = false
      break

    case 'ArrowLeft':
    case 'q':
      this.frequency.decreasing = false
      break
    default:
    }
  }

  handlePointermove (x: number, y: number, leftButtonPressed: boolean): void {
    if (!leftButtonPressed) {
      this.pointer.gap = null

      return
    }

    this.pointer.current.setValues(x, y)

    if (this.pointer.gap === null) {
      this.pointer.origin.setValues(x, y)
      this.pointer.gap = new createjs.Point(0, 0)
      this.gameInstance.character.sine.memorize()
    }
  }

  update (speed: number): void {
    if (this.gameInstance.isGameOver) {
      return
    }

    this.updateKeyboardControls(speed)
    this.updatePointerControls()
  }

  private updateKeyboardControls (speed: number): void {
    this.amplitude.increment = 0
    this.frequency.increment = 0

    if (this.amplitude.increasing) {
      this.amplitude.increment = this.amplitude.step * speed
    }
    if (this.amplitude.decreasing) {
      this.amplitude.increment = -this.amplitude.step * speed
    }
    if (this.frequency.increasing) {
      this.frequency.increment = this.frequency.step * speed
    }
    if (this.frequency.decreasing) {
      this.frequency.increment = -this.frequency.step * speed
    }
  }

  private updatePointerControls (): void {
    if (this.pointer.gap === null) {
      return
    }

    this.pointer.gap.x = this.pointer.current.x - this.pointer.origin.x
    this.pointer.gap.y = this.pointer.origin.y - this.pointer.current.y
  }
}
