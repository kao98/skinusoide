import Audio from './audio'
import Character from './character'
import GameControls from './gameControls'
import Graduations from './graduations'
import Loader from '../Loader/loader'
import ObstacleManager from './obstacleManager'

export enum DifficultyLevels {
  easy = 0,
  normal = 1,
  hard = 2
}

export default class Game {
  character: Character

  readonly controls: GameControls

  private canvas: HTMLCanvasElement
  private difficulty: DifficultyLevels
  private started: boolean
  private paused: boolean
  private stage: createjs.StageGL
  private graduations: Graduations
  private ratio: number
  private stats: Stats
  private statsElement: HTMLDivElement
  private obstacleManager: ObstacleManager
  private over: boolean
  private scoring: number
  private score: number
  private scoreElement: HTMLElement

  constructor (
    private readonly loader: Loader,
    private readonly audio: Audio,
    private debug: boolean
  ) {
    this.canvas = null
    this.started = false
    this.paused = false
    this.ratio = 0

    this.controls = new GameControls(this)

    this.scoreElement = document.getElementById('score')

    if (this.debug) {
      this.stats = new Stats()
      // 0: fps, 1: ms, 2: mb, 3+: custom
      this.stats.showPanel(0)
    }
  }

  setCanvas (canvas: HTMLCanvasElement): void {
    this.canvas = canvas
  }

  initialize (): void {
    this.initializeCanvas()

    if (this.debug) {
      this.statsElement = document.body.appendChild(this.stats.dom)
    }
  }

  start (difficulty: DifficultyLevels): void {
    this.initializeGame(difficulty)
    this.loadGame()
    this.audio.playTheme(Math.ceil(Math.random() * 3))
    this.setTicker()
  }

  pause (): void {
    createjs.Ticker.paused = true
    this.paused = true
  }

  resume (): void {
    createjs.Ticker.paused = false
    this.paused = false
  }

  stop (removeDebugInfos = true): void {
    this.started = false
    this.stage.removeAllChildren()
    this.stage.clear()

    if (removeDebugInfos && this.debug) {
      document.body.removeChild(this.statsElement)
    }
  }

  restart (): void {
    this.stop(false)
    this.start(this.difficulty)
  }

  collide (): void {
    this.character.collide()
  }

  gameOver (): void {
    this.scoring = 0
    this.over = true
    this.audio.stopTheme()
  }

  private setTicker (): void {
    createjs.Ticker.reset()
    createjs.Ticker.paused = false
    createjs.Ticker.addEventListener(
      'tick',
      (event: createjs.TickerEvent) => this.tick(event)
    )
    createjs.Ticker.timingMode = createjs.Ticker.TIMEOUT
    createjs.Ticker.framerate = 144
  }

  private initializeCanvas (): void {
    this.stage = new createjs.StageGL(this.canvas, {antialias: true})
    this.stage.snapToPixel = false

    this.resizeCanvas()

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    this.stage.setClearColor('#000000')
  }

  private initializeGame (difficulty: DifficultyLevels): void {
    this.difficulty = difficulty
    this.score = 0
    this.over = false
    this.paused = false
    this.started = true

    switch (difficulty) {
    case 0:
      this.scoring = 50
      break
    case 2:
      this.scoring = 200
      break
    default:
      this.scoring = 100
    }
  }

  private loadGame (): void {
    this.character = new Character(this)
    this.graduations = new Graduations(this, this.character.sine)
    this.obstacleManager = new ObstacleManager(this, this.stage)

    this.stage.addChild(this.character)
    this.stage.addChild(this.graduations)
  }

  private tick (tickEvent: createjs.TickerEvent): void {
    if (!this.started || this.paused) {
      return
    }

    if (this.debug) {
      this.stats.begin()
    }

    const speed = tickEvent.delta / Game.BASE_DELTA

    createjs.Tween.tick(tickEvent.delta, tickEvent.paused)
    this.update(speed)

    if (this.debug) {
      this.stats.end()
    }
  }

  resizeCanvas (): void {
    const VIEW_IDEAL_WIDTH = 1024

    this.canvas.width = this.canvas.parentElement.offsetWidth
    this.canvas.height = this.canvas.parentElement.offsetHeight

    if (this.stage) {
      this.stage.updateViewport(this.canvas.width, this.canvas.height)
    }

    this.ratio = this.canvas.width / VIEW_IDEAL_WIDTH
  }

  private update (speed: number): void {
    let score = this.scoring * speed

    this.controls.update(speed)
    this.character.update(speed)
    this.graduations.update()

    score += this.obstacleManager.update(speed) * this.scoring

    this.score += Math.round(score)

    this.stage.update()

    this.scoreElement.innerHTML = `${this.displayedScore} pts`
  }

  get isStarted (): boolean {
    return this.started
  }

  get isPaused (): boolean {
    return this.paused
  }

  get stageWidth (): number {
    return this.canvas.width
  }

  get stageHeight (): number {
    return this.canvas.height
  }

  get stageRatio (): number {
    return this.ratio
  }

  get stageCenterX (): number {
    return this.stageWidth / 2
  }

  get stageCenterY (): number {
    return this.stageHeight / 2
  }

  get isGameOver (): boolean {
    return this.over
  }

  private get displayedScore (): string {
    return this.score.toLocaleString()
  }

  get difficultyLevel (): DifficultyLevels {
    return this.difficulty
  }

  static get BASE_DELTA (): number {
    const delta = 1000 / 60

    return delta
  }
}
