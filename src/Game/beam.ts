import {HSL, HSLColor} from '../Utils/Colors'
import BeamParticle from './beamParticle'
import ParticleManager from './particleManager'

const
  COLOR = {hue: 195, sat: 100, lit: 60} as HSL,
  DENSITY = 400,
  PARTICLE_CREATION_INTERVAL = 8,
  RADIUS = 4

export default class Beam extends createjs.Container {
  private characterX: number
  private characterY: number
  private isActive: boolean
  private offset: number
  private particleManager: ParticleManager
  private sprite: createjs.Shape
  private stageRatio: number

  constructor () {
    super()

    this.isActive = true
    this.offset = 0
    this.stageRatio = 0

    this.particleManager = new ParticleManager(
      DENSITY,
      PARTICLE_CREATION_INTERVAL
    )

    this.particleManager.particleCreationMethod = this.createParticle.bind(this)
    this.particleManager.isCandidateMethod = Beam.isParticleCandidate

    this.buildSprite()
  }

  update (
    x: number,
    y: number,
    speedRatio: number,
    characterVelocity: number
  ): void {
    this.characterX = x - this.offset
    this.characterY = y - this.offset

    if (this.isActive) {
      this
        .particleManager
        .update(
          createjs.Ticker.getTime(true),
          this.characterX,
          this.characterY
        )
    }

    this
      .particleManager
      .particles
      .forEach((particle: BeamParticle) => {
        particle.update(speedRatio, this.stageRatio, characterVelocity)
      })
  }

  scale (stageRatio: number): void {
    this.stageRatio = stageRatio
    this.offset = RADIUS * stageRatio
    this.particles.forEach((particle) => {
      particle.scaleX = stageRatio
      particle.scaleY = stageRatio
    })
  }

  stop (): void {
    this.isActive = false
  }

  private createParticle (): createjs.Bitmap {
    const point = new BeamParticle(
      this.sprite.cacheCanvas,
      DENSITY * PARTICLE_CREATION_INTERVAL
    )

    point.tickEnabled = false
    point.scaleX = this.stageRatio
    point.scaleY = this.stageRatio
    point.x = this.characterX
    point.y = this.characterY

    return this.addChild(point)
  }

  private static isParticleCandidate (particle: BeamParticle): boolean {
    return particle.visible === false
  }

  private buildSprite (): void {
    const color = HSLColor.hslCssColor(COLOR)

    this.sprite = new createjs.Shape()

    this
      .sprite
      .graphics
      .beginFill(color)
      .drawCircle(0, 0, RADIUS)
      .endFill()

    this.sprite.cache(-RADIUS, -RADIUS, RADIUS * 2, RADIUS * 2)
  }

  private get particles (): Array<BeamParticle> {
    return this.particleManager.particles as Array<BeamParticle>
  }
}
