/* eslint-disable max-classes-per-file */
import {HSL, HSLColor} from '../Utils/Colors'
import Game from './game'

const DENSITY = 200

enum ExplosionParticleStyle {
  circle = 1,
  sqare = 2
}

class ExplosionParticle extends createjs.Shape {
  private color: HSL
  private style: ExplosionParticleStyle
  private velocityMultiplier: number
  private velocityR: number
  private velocityX: number
  private velocityY: number

  constructor (velocityMultiplier: number, ...args) {
    super(...args)

    this.color = ExplosionParticle.getRandomColor()
    this.style = ExplosionParticle.getRandomStyle()

    this.velocityMultiplier = velocityMultiplier
    this.velocityR = this.getRandomVelocity()
    this.velocityX = this.getRandomVelocity()
    this.velocityY = this.getRandomVelocity()

    this.drawGraphics()
  }

  update (speedRatio: number): void {
    this.x += this.velocityX * speedRatio
    this.y += this.velocityY * speedRatio
    this.rotation += this.velocityR * speedRatio
    this.alpha -= 0.008 * speedRatio

    if (this.alpha <= 0) {
      this.visible = false
    }
  }

  private static getRandomColor (): HSL {
    switch (Math.ceil(Math.random() * 3)) {
    case 1:
      return {hue: 195, sat: 100, lit: 100}
    case 2:
      return {hue: 30, sat: 90, lit: 46}
    default:
      return {hue: 195, sat: 100, lit: 60}
    }
  }

  private static getRandomStyle (): ExplosionParticleStyle {
    return Math.ceil(Math.random() * 2)
  }

  private getRandomVelocity (): number {
    const
      baseVelocity = Math.random() - 0.5,
      initialMultiplier = Math.random() * 20

    return baseVelocity * initialMultiplier * this.velocityMultiplier
  }

  private drawGraphics (): void {
    if (this.style === ExplosionParticleStyle.circle) {
      this.drawCircleShape()
    } else {
      this.drawSqareShape()
    }
  }

  private drawCircleShape (): void {
    const
      color = HSLColor.hslCssColor(this.color),
      radius = Math.random() * 3 + 2

    this.graphics.beginFill(color).drawCircle(0, 0, radius)
    this.cache(
      Math.random() * -radius,
      Math.random() * -radius,
      Math.random() * radius * 2,
      Math.random() * radius * 2
    )
  }

  private drawSqareShape (): void {
    const
      color = HSLColor.hslCssColor(this.color),
      length = Math.random() * 6 + 4,
      height = length * Math.random(),
      width = length * Math.random()

    this.graphics.beginFill(color).drawRect(0, 0, width, height)
    this.regX = width / 2
    this.regY = height / 2
    this.cache(0, 0, width, height)
  }
}

export default class Explosion extends createjs.Container {
  private particles: Array<ExplosionParticle>

  constructor (private readonly gameInstance: Game) {
    super()

    this.particles = []
    this.buildExplosion()
  }

  update (speedRatio: number): void {
    this.particles.forEach((particle) => {
      particle.update(speedRatio)
      if (!particle.visible) {
        this.removeChild(particle)
      }
    })

    if (this.children.length === 0) {
      this.gameInstance.restart()
    }
  }

  private buildExplosion (): void {
    do {
      const particle = new ExplosionParticle(
        this.gameInstance.character.sine.getFrequency()
      )

      particle.x = this.gameInstance.character.x
      particle.y = this.gameInstance.character.y

      particle.scaleX = this.gameInstance.stageRatio
      particle.scaleY = this.gameInstance.stageRatio

      this.particles.push(particle)
      this.addChild(particle)
    } while (this.particles.length < DENSITY)
  }
}
