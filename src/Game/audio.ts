import Loader from '../Loader/loader'

interface SoundInstances {
  menuTheme: createjs.AbstractSoundInstance | null,
  theme: createjs.AbstractSoundInstance | null
}

interface SoundOptions {
  interrupt?: string,

  // 0: no-loop - -1: inifinite
  loop?: number,

  // Between 0 and 1
  volume?: number,

  // Milliseconds
  delay?: number,

  // Milliseconds
  offset?: number,

  // Between -1 and 1
  pan?: number,

  // Milliseconds - for audio sprites - offset to start playback and loop from
  startTime?: number,

  duration?: number
}

export default class Audio {
  private instances: SoundInstances

  constructor (private readonly loader: Loader) {
    this.instances = {
      menuTheme: null,
      theme: null
    }
  }

  static mute (): void {
    createjs.Sound.muted = true
  }

  static unmute (): void {
    createjs.Sound.muted = false
  }

  static get muted (): boolean {
    return createjs.Sound.muted
  }

  playMenuTheme (): void {
    const config: createjs.PlayPropsConfig = Audio.createConfig({
      loop: -1,
      volume: 0.6
    })

    this.stopTheme()
    this.instances.menuTheme = createjs.Sound.play('menu-theme', config)
  }

  stopMenuTheme (): void {
    // eslint-disable-next-line prefer-reflect
    createjs.Tween
      .get(this.instances.menuTheme)
      .to({volume: 0.0}, 1000)
      .call(() => {
        this.instances.menuTheme.stop()
      })
  }

  playTheme (themeNumber: number): void {
    const
      config: createjs.PlayPropsConfig = Audio.createConfig({
        loop: -1,
        volume: 0.0
      }),
      theme = `music-${themeNumber}`

    this.instances.theme = createjs.Sound.play(theme, config)

    createjs.Tween
      .get(this.instances.theme)
      .to({volume: 0.6}, 2000)
  }

  stopTheme (): void {
    this.instances.theme?.stop()
  }

  static playFx (id: string): void {
    const
      config: createjs.PlayPropsConfig = Audio.createConfig({
        interrupt: createjs.Sound.INTERRUPT_NONE,
        volume: 0.8
      })

    createjs.Sound.play(id, config)
  }

  private static createConfig (options: SoundOptions)
  : createjs.PlayPropsConfig {
    /* eslint-disable no-magic-numbers */
    return new createjs.PlayPropsConfig().set({
      delay: options.delay || 0,
      duration: options.duration || null,
      interrupt: options.interrupt || createjs.Sound.INTERRUPT_ANY,
      loop: options.loop || 0,
      offset: options.offset || 0,
      pan: options.pan || 0,
      startTime: options.startTime || null,
      volume: options.volume ?? 0.8
    })
    /* eslint-enable no-magic-numbers */
  }
}
