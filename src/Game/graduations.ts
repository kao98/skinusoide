import Game, {DifficultyLevels} from './game'
import Sine from './sine'

const
  GRADUATION_STYLE = 2,
  GRADUATION_LENGTH = 32

export default class Graduations extends createjs.Container {
  private currentXGraduation: createjs.Shape
  private lowerXGraduation: createjs.Shape
  private higherXGraduation: createjs.Shape
  private xGraduationRealPosition: number
  private xGraduationHeight: number

  private lowerYGraduation: createjs.Shape
  private higherYGraduation: createjs.Shape
  private yGraduationWidth: number

  private previousRatio: number

  constructor (
    private readonly gameInstance: Game,
    private readonly sine: Sine
  ) {
    super()

    this.xGraduationHeight =
      this.gameInstance.difficultyLevel === DifficultyLevels.easy
        ? this.gameInstance.stageHeight
        : GRADUATION_LENGTH

    this.yGraduationWidth =
      this.gameInstance.difficultyLevel === DifficultyLevels.easy
        ? this.gameInstance.stageWidth
        : GRADUATION_LENGTH

    this.previousRatio = 0
    this.xGraduationRealPosition = 0
    this.buildGraduations()
  }

  update (): void {
    if (!this.visible) {
      return
    }

    this.updateXRealPosition()
    this.updateXGraduationsX()
    this.updateYGraduationsY()

    if (this.previousRatio !== this.gameInstance.stageRatio) {
      this.fixXGraduationsY()
      this.fixYGraduationsX()
      this.previousRatio = this.gameInstance.stageRatio
    }
  }

  private buildGraduations (): void {
    if (this.gameInstance.difficultyLevel === DifficultyLevels.hard) {
      this.visible = false

      return
    }

    this.currentXGraduation = this.buildGraduation()
    this.lowerXGraduation = this.buildGraduation()
    this.higherXGraduation = this.buildGraduation()
    this.lowerYGraduation = this.buildGraduation(true)
    this.higherYGraduation = this.buildGraduation(true)

    this.cacheShapes()

    this.addChild(
      this.currentXGraduation,
      this.lowerXGraduation,
      this.higherXGraduation,
      this.lowerYGraduation,
      this.higherYGraduation
    )
  }

  updateXRealPosition (): void {
    this.xGraduationRealPosition = this.gameInstance.character.x
  }

  updateXGraduationsX (): void {
    const
      {stageRatio} = this.gameInstance,
      currentX = Math.round(this.xGraduationRealPosition)

    this.currentXGraduation.x = currentX

    this.lowerXGraduation.x =
      Math.floor(currentX - this.sine.getVisualFrequency(stageRatio))

    this.higherXGraduation.x =
      Math.ceil(currentX + this.sine.getVisualFrequency(stageRatio))
  }

  fixXGraduationsY (): void {
    let height = this.xGraduationHeight

    if (height < this.gameInstance.stageHeight) {
      height *= this.gameInstance.stageRatio
      this.lowerXGraduation.scaleY = this.gameInstance.stageRatio
      this.higherXGraduation.scaleY = this.gameInstance.stageRatio
      this.currentXGraduation.scaleY = this.gameInstance.stageRatio
    }

    this.currentXGraduation.y = this.gameInstance.stageHeight
    this.currentXGraduation.y -= height

    this.lowerXGraduation.y = this.currentXGraduation.y
    this.higherXGraduation.y = this.currentXGraduation.y
  }

  updateYGraduationsY (): void {
    const
      origin = this.gameInstance.stageCenterY,
      ratio = this.gameInstance.stageRatio

    this.higherYGraduation.y =
      Math.floor(origin - this.sine.getVisualAmplitude(ratio))

    this.lowerYGraduation.y =
      Math.ceil(origin + this.sine.getVisualAmplitude(ratio))
  }

  fixYGraduationsX (): void {
    let width = this.yGraduationWidth

    if (width < this.gameInstance.stageWidth) {
      width *= this.gameInstance.stageRatio
    }

    const offset = width

    this.higherYGraduation.x = this.gameInstance.stageWidth - offset
    this.lowerYGraduation.x = this.higherYGraduation.x
  }

  private buildGraduation (yGraduation = false): createjs.Shape {
    const
      graduation = new createjs.Shape()

    let
      lineToX = 0,
      lineToY = 0

    if (yGraduation) {
      lineToX = this.yGraduationWidth
    } else {
      lineToY = this.xGraduationHeight
    }

    graduation
      .graphics
      .beginStroke('green')
      .setStrokeStyle(GRADUATION_STYLE)
      .moveTo(0, 0)
      .lineTo(lineToX, lineToY)
      .endStroke()

    graduation.tickEnabled = false

    return graduation
  }

  private cacheShapes (): void {
    [
      this.currentXGraduation,
      this.lowerXGraduation,
      this.higherXGraduation
    ].forEach((graduation) => {
      graduation.cache(
        graduation.x,
        graduation.y,
        GRADUATION_STYLE,
        this.xGraduationHeight
      )
    });

    [
      this.higherYGraduation,
      this.lowerYGraduation
    ].forEach((graduation) => {
      graduation.cache(
        graduation.x,
        graduation.y,
        this.yGraduationWidth,
        GRADUATION_STYLE
      )
    })
  }
}
