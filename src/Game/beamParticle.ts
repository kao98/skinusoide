export default class BeamParticle extends createjs.Bitmap {
  private birthTime: number
  private lifeTime: number
  private velocity: number

  constructor (imageOrUrl: unknown, lifeTime: number) {
    super(imageOrUrl)

    this.birthTime = createjs.Ticker.getTime(true)
    this.lifeTime = lifeTime
    this.velocity = 1
  }

  update (
    speedRatio: number,
    stageRatio: number,
    characterVelocity: number
  ): void {
    if (!this.visible) {
      return
    }

    const
      age = createjs.Ticker.getTime(true) - this.birthTime,
      velocity = this.velocity - characterVelocity

    this.x -= velocity * speedRatio * stageRatio

    if (age >= this.lifeTime) {
      this.alpha = 0
    } else {
      this.alpha = 1 - age / this.lifeTime
    }

    if (this.alpha <= 0 || this.x <= 0) {
      this.visible = false
    }
  }

  reset (x: number, y: number): void {
    this.birthTime = createjs.Ticker.getTime(true)
    this.alpha = 1
    this.visible = true
    this.x = x
    this.y = y
  }
}
