import {HSL, HSLColor} from '../Utils/Colors'
import Game from './game'

const COLOR = {hue: 30, sat: 90, lit: 46} as HSL

export default class Obstacle extends createjs.Container {
  private sprite: createjs.DisplayObject
  private length: number
  private spacing: number
  private thickness: number
  private numberOfBars: number
  private previousRatio: number
  private rotationSpeed: number
  private linearSpeed: number

  constructor (private readonly gameInstance: Game, previousY: number) {
    super()

    this.previousRatio = 0
    this.tickEnabled = false
    this.reset(null, previousY)
  }

  update (speedRatio: number): number {
    if (this.isGone()) {
      return 0
    }

    this.scale()

    // eslint-disable-next-line no-magic-numbers
    this.rotation = (this.rotation + this.rotationSpeed * speedRatio) % 360
    this.x -= this.linearSpeed * speedRatio * this.gameInstance.stageRatio

    this.detectCollision()

    if (this.isOutOfBounds()) {
      this.visible = false

      return this.score
    }

    return 0
  }

  reset (_x?: number, y?: number): void {
    this.buildObstacle(y)
    this.visible = true
  }

  isGone (): boolean {
    return !this.visible
  }

  private buildObstacle (lastY: number): void {
    this.computeProperties()

    const shape = this.drawGraphics()

    this.sprite = new createjs.Bitmap(shape.cacheCanvas)
    this.sprite.tickEnabled = false

    this.removeAllChildren()
    this.addChild(this.sprite)

    this.x = this.gameInstance.stageWidth + this.length
    this.y = this.computeRandomY(lastY)
  }

  private isOutOfBounds (): boolean {
    return this.x < -this.getBounds().width
  }

  private computeProperties (): void {
    this.numberOfBars = Obstacle.randomNumberOfBars
    this.spacing = Obstacle.randomSpacing
    this.thickness = Obstacle.randomThickness
    this.linearSpeed = Obstacle.randomLinearSpeed

    this.length = Obstacle.computeRandomLength(this.numberOfBars)
    this.rotationSpeed = Obstacle.computeRotationSpeed(this.length)

    this.rotation = Obstacle.computeInitialRotation(
      this.gameInstance.character.sine.getVisualFrequency(1)
    )

    // eslint-disable-next-line no-magic-numbers
    this.regX = Math.random() * this.length / 2 + this.length / 4
    // eslint-disable-next-line no-magic-numbers
    this.regY = Math.random() * this.height / 2 + this.height / 4
  }

  private scale (): void {
    if (this.previousRatio !== this.gameInstance.stageRatio) {
      this.scaleX = this.gameInstance.stageRatio
      this.scaleY = this.gameInstance.stageRatio
      this.previousRatio = this.gameInstance.stageRatio
    }
  }

  private detectCollision (): void {
    const collisionBox = this.globalToLocal(
      this.gameInstance.character.x,
      this.gameInstance.character.y
    )

    if (this.hitTest(collisionBox.x, collisionBox.y)) {
      this.gameInstance.collide()
    }
  }

  private drawGraphics (): createjs.Shape {
    const
      blurX = 2,
      blurY = 2,
      blurQuality = 2,
      blurFilter = new createjs.BlurFilter(blurX, blurY, blurQuality),
      color = HSLColor.hslCssColor(COLOR),
      filterBounds = blurFilter.getBounds(),
      shape = new createjs.Shape()

    let line = 0

    shape.graphics
      .clear()
      .setStrokeStyle(this.thickness)
      .beginStroke(color)

    do {
      shape.graphics
        .moveTo(0, line * (this.spacing + this.thickness))
        .lineTo(this.length, line * (this.spacing + this.thickness))
    } while (++line < this.numberOfBars)

    shape.graphics.endStroke()
    shape.filters = [blurFilter]

    shape.cache(
      -filterBounds.x,
      Math.min(-this.thickness / 2, -filterBounds.y),
      this.length + filterBounds.width,
      this.height + filterBounds.height
    )

    return shape
  }

  private computeRandomY (previousY: number): number {
    const
      ratio = this.gameInstance.stageRatio,
      amplitude = this.gameInstance.character.sine.getVisualAmplitude(ratio),
      randomY = Math.random() * amplitude,
      center = this.gameInstance.stageCenterY,
      lucky = Math.random() < 0.5

    let luckFactor = 0

    switch (true) {
    case previousY > center:
      luckFactor = lucky ? 0 : -1
      break
    case previousY < center:
      luckFactor = lucky ? 0 : 1
      break
    default:
      luckFactor = lucky ? -1 : 1
    }

    return center + randomY * luckFactor
  }

  private get height (): number {
    return this.numberOfBars * (this.spacing + this.thickness)
  }

  private static get randomThickness (): number {
    const
      base = 3,
      minimum = 6

    return Math.floor(Math.random() * base + minimum)
  }

  private static get randomSpacing (): number {
    const
      base = 5,
      minimum = 2

    return Math.floor(Math.random() * base + minimum)
  }

  private static computeRandomLength (numberOfBars: number): number {
    const
      base = 50,
      minimum = 50

    return Math.floor(Math.random() * base * numberOfBars + minimum)
  }


  private static computeRotationSpeed (length: number): number {
    const
      base = 3,
      maximalLength = 100

    if (length >= maximalLength) {
      return 0
    }

    return Math.random() * base
  }

  private static computeInitialRotation (sineFrequency: number): number {
    const
      barrierFrequency = 120,
      // eslint-disable-next-line no-magic-numbers
      initialValue = Math.random() * 90

    /**
     * The orientation of the obstacle depends on the speed of the character.
     * Fast character have obstacle not that much rotated for them to be longer
     * Slow character must face obstacle that are more high
     */

    if (sineFrequency < barrierFrequency) {
      // eslint-disable-next-line no-magic-numbers
      return Math.floor(initialValue - 45)
    }

    // eslint-disable-next-line no-magic-numbers
    return Math.floor(initialValue + 45)
  }

  private static get randomNumberOfBars (): number {
    const
      base = 3,
      minimum = 2

    return Math.floor(Math.random() * base + minimum)
  }

  private static get randomLinearSpeed (): number {
    const
      base = 3,
      minimum = 1

    return Math.floor(Math.random() * base + minimum)
  }

  private get score (): number {
    return this.length
  }
}
