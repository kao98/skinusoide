import Game from './game'

const
  AMPLITUDE_MAX = 250,
  AMPLITUDE_MIN = 30,
  FREQUENCY_INCREMENT_FACTOR = 7,
  FREQUENCY_MAX = 0.8,
  FREQUENCY_MIN = 0.055,
  INCREMENT = 10,
  INITIAL_AMPLITUDE = 100,
  INITIAL_FREQUENCY = 0.4

export default class Sine {
  private angle: number
  private amplitude: number
  private frequency: number
  private memorizedAmplitude: number
  private memorizedFrequency: number

  // eslint-disable-next-line no-useless-constructor, no-empty-function
  constructor (private readonly gameInstance: Game) {
    this.amplitude = INITIAL_AMPLITUDE
    this.frequency = INITIAL_FREQUENCY

    this.angle = 0
    this.memorizedAmplitude = 0
    this.memorizedFrequency = 0
  }

  update (speedRatio: number): void {
    this.updateAmplitude()
    this.updateFrequency()
    this.updateAngle(speedRatio)
  }

  memorize (): void {
    this.memorizedAmplitude = this.amplitude
    this.memorizedFrequency = this.frequency
  }

  private updateAmplitude (): void {
    this.amplitude += this.gameInstance.controls.amplitude.increment

    if (this.gameInstance.controls.pointer.gap) {
      this.updateAmplitudeFromPointer(this.gameInstance.controls.pointer.gap.y)
      this.updateFrequencyFromPointer(this.gameInstance.controls.pointer.gap.x)
    }

    this.amplitude = Sine.clamp(
      this.amplitude,
      AMPLITUDE_MIN,
      AMPLITUDE_MAX
    )
  }

  private updateFrequency (): void {
    const
      {increment} = this.gameInstance.controls.frequency,
      multiplier = this.frequency * FREQUENCY_INCREMENT_FACTOR

    this.frequency += increment * multiplier

    this.frequency = Sine.clamp(
      this.frequency,
      FREQUENCY_MIN,
      FREQUENCY_MAX
    )
  }

  private updateAngle (speedRatio: number): void {
    // eslint-disable-next-line no-magic-numbers
    if (this.angle >= 360) {
      this.angle = -this.angle
    }

    this.angle += this.getSpeed(speedRatio)
  }

  private updateAmplitudeFromPointer (gap: number): void {
    let newAmplitude = 0

    if (gap > 0) {
      const percent =
        gap / this.gameInstance.controls.pointer.origin.y

      newAmplitude = this.increaseAmplitudeBy(percent)
    } else if (gap < 0) {
      const percent =
        -gap /
        (
          this.gameInstance.stageHeight -
          this.gameInstance.controls.pointer.origin.y
        )

      newAmplitude = this.decreaseAmplitudeBy(percent)
    }

    if (newAmplitude !== 0) {
      this.amplitude = newAmplitude
    }
  }

  private updateFrequencyFromPointer (gap: number): void {
    let newFrequency = 0

    if (gap > 0) {
      const percent =
        gap /
        (
          this.gameInstance.stageWidth -
          this.gameInstance.controls.pointer.origin.x
        )

      newFrequency = this.increaseFrequencyBy(percent)
    } else if (gap < 0) {
      const percent =
        -gap / this.gameInstance.controls.pointer.origin.x

      newFrequency = this.decreaseFrequencyBy(percent)
    }

    if (newFrequency !== 0) {
      this.frequency = newFrequency
    }
  }

  private increaseAmplitudeBy (percent: number): number {
    return Sine.increaseNumberBy(
      this.memorizedAmplitude,
      AMPLITUDE_MAX,
      percent
    )
  }

  private decreaseAmplitudeBy (percent: number): number {
    return Sine.decreaseNumberBy(
      this.memorizedAmplitude,
      AMPLITUDE_MIN,
      percent
    )
  }

  private increaseFrequencyBy (percent: number): number {
    return Sine.increaseNumberBy(
      this.memorizedFrequency,
      FREQUENCY_MAX,
      percent
    )
  }

  private decreaseFrequencyBy (percent: number): number {
    return Sine.decreaseNumberBy(
      this.memorizedFrequency,
      FREQUENCY_MIN,
      percent
    )
  }

  private static increaseNumberBy (
    number: number,
    base: number,
    percent: number
  ): number {
    const
      gap = base - number,
      increment = gap * percent

    return number + increment
  }

  private static decreaseNumberBy (
    number: number,
    base: number,
    percent: number
  ): number {
    const
      gap = number - base,
      increment = gap * percent

    return number - increment
  }

  private static clamp (value: number, min: number, max: number): number {
    if (value < min) {
      return min
    }

    if (value > max) {
      return max
    }

    return value
  }

  public get y (): number {
    // eslint-disable-next-line no-magic-numbers
    const y = Math.sin(this.angle * Math.PI / 180)

    return y * this.amplitude
  }

  public getVisualAmplitude (ratio: number): number {
    return this.amplitude * ratio
  }

  public getVisualFrequency (ratio: number): number {
    // eslint-disable-next-line no-magic-numbers
    let result = 360 / INCREMENT

    result /= this.frequency

    return result * ratio
  }

  public getFrequency (): number {
    return this.frequency
  }

  private getSpeed (speedRatio: number): number {
    return this.frequency * INCREMENT * speedRatio
  }
}
