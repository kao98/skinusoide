import Game, {DifficultyLevels} from './game'
import Obstacle from './obstacle'
import ParticleManager from './particleManager'

const MAX = 10

export default class ObstacleManager {
  private particleManager: ParticleManager
  private lastY: number

  constructor (
    private readonly gameInstance: Game,
    private readonly stage: createjs.StageGL
  ) {
    this.lastY = 0
    this.particleManager = new ParticleManager(MAX)

    this.particleManager.particleCreationMethod = this.createObstacle.bind(this)
    this.particleManager.isCandidateMethod = ObstacleManager.isCandidate
  }

  update (speedRatio: number): number {
    const updatedObstacle =
      this
        .particleManager
        .update(createjs.Ticker.getTime(true), null, this.lastY)

    if (updatedObstacle) {
      this.lastY = (updatedObstacle as Obstacle).y
    }

    let score = 0

    this
      .particleManager
      .particles
      .forEach(
        (obstacle: Obstacle) => {
          score += obstacle.update(speedRatio)
        }
      )

    return score
  }

  private createObstacle (): Obstacle {
    const obstacle = new Obstacle(this.gameInstance, this.lastY)

    this.lastY = obstacle.y
    this.particleManager.nextDelay = ObstacleManager.computeRandomDelay(
      this.gameInstance.difficultyLevel
    )

    return this.stage.addChild(obstacle)
  }

  private static isCandidate (obstacle: Obstacle): boolean {
    return obstacle.isGone()
  }

  private static computeRandomDelay (difficulty: DifficultyLevels): number {
    const baseDelay = 6000
    let minimumDelay = 0

    switch (difficulty) {
    case DifficultyLevels.easy:
      minimumDelay = 4000
      break
    case DifficultyLevels.normal:
      minimumDelay = 2000
      break
    default:
      minimumDelay = 0
    }

    return Math.floor(Math.random() * baseDelay) + minimumDelay
  }
}
