import {HSL, HSLColor} from '../Utils/Colors'
import Audio from './audio'
import Beam from './beam'
import Explosion from './explosion'
import Game from './game'
import Sine from './sine'

const
  COLOR = {hue: 195, sat: 100, lit: 100} as HSL,
  SIZE = 4

export default class Character extends createjs.Container {
  readonly sine: Sine

  private beam: Beam
  private explosion: Explosion
  private isExploding: boolean
  private previousRatio: number
  private velocity: number

  constructor (private readonly gameInstance: Game) {
    super()

    this.isExploding = false
    this.previousRatio = 0
    this.velocity = 1

    this.sine = new Sine(this.gameInstance)

    this.buildCharacter()

    this.buildBeam()
  }

  update (speedRatio: number): void {
    if (this.isExploding) {
      this.updateExplosion(speedRatio)
    } else {
      this.updateCharacter(speedRatio)
    }

    this.beam?.update(this.x, this.y, speedRatio, this.velocity)
  }

  collide (): void {
    if (this.gameInstance.isGameOver) {
      return
    }

    this.visible = false
    this.explode()
    this.beam?.stop()
    this.gameInstance.gameOver()
  }

  private buildCharacter (): void {
    const
      blurFilter = new createjs.BlurFilter(SIZE, SIZE, 2),
      filterBounds = blurFilter.getBounds(),
      shape = new createjs.Shape()

    shape
      .graphics
      .beginFill(HSLColor.hslCssColor(COLOR))
      .drawCircle(0, 0, SIZE)
      .endFill()

    shape.filters = [blurFilter]
    shape.tickEnabled = false

    shape.cache(
      -SIZE + filterBounds.x,
      -SIZE + filterBounds.y,
      (2 * SIZE) + filterBounds.width,
      (2 * SIZE) + filterBounds.height
    )

    this.tickEnabled = false
    this.addChild(shape)
  }

  private buildBeam (): void {
    this.beam = new Beam()

    if (this.stage) {
      this.stage.addChildAt(this.beam, this.stage.getChildIndex(this))
    } else {
      this.on(
        'added',
        () => this.stage.addChildAt(this.beam, this.stage.getChildIndex(this))
      )
    }
  }

  private updateX (speedRatio: number): void {
    const
      TIER = 3,
      maxX = this.gameInstance.stageWidth / TIER

    if (this.x < maxX) {
      this.velocity = 1
      this.x += this.velocity * speedRatio * this.gameInstance.stageRatio
    } else {
      this.velocity = 0
      this.x = maxX
    }
  }

  private updateCharacter (speedRatio: number): void {
    this.sine.update(speedRatio)
    this.updateX(speedRatio)

    this.y = this.sine.y * this.gameInstance.stageRatio
    this.y += this.gameInstance.stageCenterY

    if (this.previousRatio !== this.gameInstance.stageRatio) {
      this.scaleX = this.gameInstance.stageRatio
      this.scaleY = this.gameInstance.stageRatio
      this.beam.scale(this.gameInstance.stageRatio)

      this.previousRatio = this.gameInstance.stageRatio
    }
  }

  private updateExplosion (speedRatio: number): void {
    this.explosion?.update(speedRatio)
  }

  private explode (): void {
    if (this.isExploding) {
      return
    }

    this.explosion = new Explosion(this.gameInstance)
    this.stage.addChild(this.explosion)
    Audio.playFx('explosion')
    this.isExploding = true
  }
}
