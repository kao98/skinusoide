import Game, {DifficultyLevels} from '../Game/game'
import Audio from '../Game/audio'
import InputsManager from './inputs-manager'
import Loader from '../Loader/loader'
import MenuManager from './menu-manager'
import UIHandlers from './ui-handlers'

export default class UI {
  readonly menuManager: MenuManager
  readonly canvas: HTMLCanvasElement

  private inputsManager: InputsManager
  private progressBarElement: HTMLElement
  private uiHandlers: UIHandlers

  constructor (
    private readonly document: HTMLDocument,
    private readonly loader: Loader,
    readonly gameInstance: Game,
    private readonly audio: Audio
  ) {
    this.menuManager = new MenuManager(this.document)
    this.inputsManager = new InputsManager(this.document)
    this.uiHandlers = new UIHandlers(this)

    this.canvas = this.document.querySelector('#mainCanvas')
    this.canvas.oncontextmenu = (event: Event) => {
      event.preventDefault()
      event.stopPropagation()
    }

    this.gameInstance.setCanvas(this.canvas)

    this.setup()
  }

  setup (): void {
    this.setupMenus()
    this.setupInputs()

    this.pauseGameWhenLosingFocus()
    this.resizeCanvasOnResize()
    this.loadUI()
  }

  setupMenus (): void {
    this.menuManager.setupMenus()

    this.menuManager.on(
      'start',
      (difficulty: DifficultyLevels) => this.start(difficulty)
    )
    this.menuManager.on('pause', () => this.pause())
    this.menuManager.on('resume', () => this.resume())
    this.menuManager.on('quit', () => this.quit())
    this.menuManager.on('toggleMute', () => UI.toggleMute())
  }

  setupInputs (): void {
    this.inputsManager.on(
      'keydown',
      (key: string, code: string) => this.uiHandlers.handleKeydown(key, code)
    )

    this.inputsManager.on(
      'keyup',
      (key: string, code: string) => this.uiHandlers.handleKeyup(key, code)
    )

    this.inputsManager.on(
      'mousemove',
      (
        target: EventTarget,
        pointerX: number,
        pointerY: number,
        buttons: number
      ) => this.uiHandlers.handleMousemove(target, pointerX, pointerY, buttons)
    )

    this.inputsManager.on(
      'touchmove',
      (
        target: EventTarget,
        touches: TouchList
      ) => this.uiHandlers.handleTouchmove(target, touches)
    )
  }

  pauseGameWhenLosingFocus (): void {
    this.document.addEventListener(
      'visibilitychange',
      () => {
        if (this.document.hidden && this.gameStarted) {
          this.menuManager.pauseGame()
        }
      },
      false
    )

    this.document.addEventListener(
      'blur',
      () => {
        if (this.gameStarted) {
          this.menuManager.pauseGame()
        }
      }
    )
  }

  resizeCanvasOnResize (): void {
    window.addEventListener(
      'resize',
      () => {
        if (this.gameStarted) {
          this.gameInstance.resizeCanvas()
        }
      },
      false
    )
  }

  loadUI (): void {
    this.showProgress()
    this.loadUiAssets().then(() => {
      if (localStorage.getItem('muted')) {
        this.menuManager.toggleMute()
      }

      this.audio.playMenuTheme()
      this.hideProgress()
      this.menuManager.showMainMenu()
    })
  }

  // Waiting for level management to be added
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  start (difficulty: DifficultyLevels): void {
    this.showProgress()
    this.gameInstance.initialize()
    this.loadGameAssets().then(() => {
      this.hideProgress()
      this.audio.stopMenuTheme()
      this.gameInstance.start(difficulty)
    })
  }

  pause (): void {
    this.gameInstance.pause()
  }

  resume (): void {
    this.gameInstance.resume()
  }

  quit (): void {
    this.gameInstance.stop()
    this.audio.playMenuTheme()
  }

  static toggleMute (): void {
    if (Audio.muted) {
      Audio.unmute()
      localStorage.removeItem('muted')
    } else {
      Audio.mute()
      localStorage.setItem('muted', 'muted')
    }
  }

  showProgress (): void {
    this.progressBar.style.display = 'block'
  }

  hideProgress (): void {
    this.progressBar.style.display = 'none'
  }

  loadUiAssets (): Promise<void> {
    return this.loader
      .setProgressCallback((
        loaded: number,
        progress: number,
        total: number
      ): void => this.updateProgress(loaded, progress, total))
      .loadManifest('manifests/ui.json')
  }

  loadGameAssets (): Promise<void> {
    return this.loader
      .setProgressCallback((
        loaded: number,
        progress: number,
        total: number
      ): void => this.updateProgress(loaded, progress, total))
      .loadManifest('manifests/game.json')
  }

  updateProgress (loaded: number, progress: number, total: number): void {
    if (!this.progressBar) {
      return
    }

    // eslint-disable-next-line no-magic-numbers
    this.progressBar.style.width = `${Math.round(progress * 100)}%`
  }

  get gameStarted (): boolean {
    return this.gameInstance.isStarted
  }

  get gamePaused (): boolean {
    return this.gameStarted && this.gameInstance.isPaused
  }

  get gameRunning (): boolean {
    return this.gameStarted && !this.gamePaused
  }

  get progressBar (): HTMLElement {
    this.progressBarElement =
      this.progressBarElement || this.document.querySelector('#progressBar')

    return this.progressBarElement
  }
}
