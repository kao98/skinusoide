export default interface IListener {
  [key: string]: Array<(...args: unknown[]) => void>
}
