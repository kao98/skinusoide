import IListener from './ilistener'

interface IKeyPressedCollection {
  [key: string]: boolean
}

export default class InputsManager {
  private listeners: IListener
  private keyPressed: IKeyPressedCollection

  constructor (private readonly document: HTMLDocument) {
    this.listeners = {
      keydown: [],
      keyup: [],
      mousemove: [],
      touchmove: []
    }

    this.keyPressed = {}

    this.setHandlers()
  }

  setHandlers (): void {
    this.setKeyboardHandlers()
    this.setTouchHandlers()
    this.setMouseHandlers()
  }

  setKeyboardHandlers (): void {
    this.document.addEventListener(
      'keydown',
      (event: KeyboardEvent) => this.keydownHandler(event.key, event.code)
    )

    this.document.addEventListener(
      'keyup',
      (event: KeyboardEvent) => this.keyupHandler(event.key, event.code)
    )
  }

  setTouchHandlers (): void {
    this.document.addEventListener(
      'touchmove',
      (event: TouchEvent) => {
        this.touchmoveHandler(event.target, event.touches)
        event.preventDefault()
      }
    )

    this.document.addEventListener(
      'touchend',
      (event: TouchEvent) => {
        this.touchendHandler(event.target)
        event.preventDefault()
      }
    )
  }

  setMouseHandlers (): void {
    this.document.addEventListener(
      'mousemove',
      (event: MouseEvent) => this.mousemoveHandler(
        event.target,
        event.offsetX,
        event.offsetY,
        event.buttons
      )
    )
  }

  keydownHandler (key: string, code: string): void {
    if (this.keyPressed[code]) {
      return
    }

    this.keyPressed[code] = true
    this.listeners.keydown.forEach((callback) => callback(key, code))
  }

  keyupHandler (key: string, code: string): void {
    Reflect.deleteProperty(this.keyPressed, code)
    this.listeners.keyup.forEach((callback) => callback(key, code))
  }

  touchmoveHandler (
    target: EventTarget,
    touches: TouchList
  ): void {
    this
      .listeners
      .touchmove
      .forEach((callback) => callback(target, touches))
  }

  touchendHandler (
    target: EventTarget
  ): void {
    this
      .listeners
      .touchmove
      .forEach((callback) => callback(target, []))
  }

  mousemoveHandler (
    target: EventTarget,
    x: number,
    y: number,
    buttons: number
  ): void {
    this
      .listeners
      .mousemove
      .forEach((callback) => callback(target, x, y, buttons))
  }

  on (event: string, callback: (...args: unknown[]) => void): void {
    this.listeners[event].push(callback)
  }
}
