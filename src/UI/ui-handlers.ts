import UI from './ui'

export default class UIHandlers {
  constructor (private readonly ui: UI) {}

  handleKeydown (key: string, code: string): void {
    switch (key.toLowerCase()) {
    case 'escape':
      break
    default:
      if (!this.ui.gameRunning) {
        return
      }

      this.ui.gameInstance.controls.handleKeydown(key, code)
    }
  }

  handleKeyup (key: string, code: string): void {
    switch (key.toLowerCase()) {
    case 'escape':
      if (!this.ui.gameStarted) {
        return
      }

      if (this.ui.gamePaused) {
        this.ui.menuManager.resumeGame()

        return
      }

      this.ui.menuManager.pauseGame()

      break
    default:
      if (!this.ui.gameRunning) {
        return
      }

      this.ui.gameInstance.controls.handleKeyup(key, code)
    }
  }

  handleMousemove (
    target: EventTarget,
    pointerX: number,
    pointerY: number,
    buttons: number
  ): void {
    if (target !== this.ui.canvas) {
      return
    }

    if (!this.ui.gameRunning) {
      return
    }

    this.ui.gameInstance.controls.handlePointermove(
      pointerX,
      pointerY,
      buttons % 2 === 1
    )
  }

  handleTouchmove (
    target: EventTarget,
    touches: TouchList
  ): void {
    if (target !== this.ui.canvas) {
      return
    }

    if (!this.ui.gameRunning) {
      return
    }

    if (touches.length > 0) {
      this.ui.gameInstance.controls.handlePointermove(
        touches[0].pageX,
        touches[0].pageY,
        true
      )
    } else {
      this.ui.gameInstance.controls.handlePointermove(0, 0, false)
    }
  }
}
