import BaseMenu from './base-menu'

export default class LevelMenu extends BaseMenu {
  protected readonly selector: string = '#levelSelectionScreen'

  protected setupMenu (): void {
    this.addMenuItem('startEasy', this.startEasyButton)
    this.addMenuItem('startNormal', this.startNormalButton)
    this.addMenuItem('startHard', this.startHardButton)
    this.addMenuItem('cancel', this.cancelButton)
  }

  get startEasyButton (): HTMLElement {
    return this.get('#startEasyButton')
  }

  get startNormalButton (): HTMLElement {
    return this.get('#startNormalButton')
  }

  get startHardButton (): HTMLElement {
    return this.get('#startHardButton')
  }

  get cancelButton (): HTMLElement {
    return this.get('#cancelLevelSelectionButton')
  }
}
